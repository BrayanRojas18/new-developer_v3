# Test RebajasTusCuentas

### 1
- **RFTM**: I have not used it
- **LMGTFY**: I have not used it
- **OS**: windows 10 or 11 
- **Languages**: 
    - Spanish: Native
    - English: A2-B1: Pre-intermediate.
***
### 2
- CI: Continuous integration is a highly recommended    development practice, which allows developers to contribute and collaborate on a code base, that is, combining code changes in a central repository, after which version changes are executed and automated tests.
    - Plan: Organization and design of work.
    - Code: Write and develop code.
    - Build: Prepare the code for the server.
    - Test: Make sure the code works (unit tests).
    - Merge: final union of the code to the base repository (GitLab).
Pd: One of the most used programs for automated testing is Selenium
***
### 3
- **Project 1**: [Luck and play](https://github.com/brayanrojas18/luck-and-play) 
- **Project 2**: Folder **./Portfolio/app-portal**
***
### 4
- **Example required**
    ```
    ###JavaScript###
    let numbers = []
    const sum = 24

    for(let num = 1; num <= 1000000; num++) {
        numbers.push(num)
    }

    function FindPair() {
        for(let i=0; i<numbers.length-2; i++){
            for(let j=i+1; j<=numbers.length-1; j++){
                if((numbers[i]+numbers[j]) === sum){
                    return `(${numbers[i]} + ${numbers[j]}= ${numbers[j]})`;
                }
            }
        }
    }

    console.log('The pair numbers: ', FindPair())
    ```

- **Example listing two by two**
    ```
    ###JavaScript###
    let numbers = []
    const sum = 24

    for(let num = 1; num <= 1000000; num++) {
        numbers.push(num)
    }

    function FindPair() {
        for(let i=0; i<numbers.length-2; i++){
            for(let j=i+1; j<=numbers.length-1; j++){
                if((numbers[i]+numbers[j]) === sum){
                    return `(${numbers[i]} + ${numbers[j]}= ${sum})`;
                }
            }
        }
    }

    console.log('The pair numbers: ', FindPair())
    ```
- **Another case could be an array with numbers in pairs.**
    ```
        let numbers = []
        const sum = 24

        for(let num = 1; num <= 1000000; num++) {
            if (num % 2 == 0) {
                numbers.push(num)
            }
        }

        function FindPair() {
            for(let i=0; i<numbers.length-2; i++){
                for(let j=i+1; j<=numbers.length-1; j++){
                    if((numbers[i]+numbers[j]) === sum){
                        return `(${numbers[i]} + ${numbers[j]}= ${sum})`;
                    }
                }
            }
        }

        console.log('The pair numbers: ', FindPair())
    ```
***
### 5
- **I don't understand task 5**
***