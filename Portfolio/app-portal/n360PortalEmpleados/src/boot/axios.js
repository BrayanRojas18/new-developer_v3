import axios from "axios";
import env from "./env";
import { Notify, LocalStorage } from "quasar";

const axiosInstance = axios.create({
  baseURL: env.apiUrl,
  headers: {
    "Access-Control-Allow-Origin": env.apiUrl,
  },
});

export default async ({ store, Vue, router }) => {
  Vue.prototype.$api = axiosInstance;
  Vue.prototype.$env = env;

  axiosInstance.interceptors.response.use(
    function (response) {
      return response.data;
    },
    function (error) {
      var data = error;
      if (error.response) {
        data = error.response.data.error;
        Notify.create(data)
      }

      return Promise.reject(data);
    }
  );

  axiosInstance.interceptors.request.use(
    async function (config) {
      const token = LocalStorage.getItem("token");
      if (token) {
        if (!config.headers) config.headers = {};
        if (token) config.headers["access-token"] = token;
      }

      const selected = LocalStorage.getItem("selected");
      if (typeof selected == "object") {
        if (!config.params) config.params = {};
        if (!config.params.hasOwnProperty("config"))
          config.params["config"] = {};
        if (selected) config.params["config"] = selected;
      }
      return config;
    },
    function (error) {
      return Promise.reject(error);
    }
  );
};

export { axiosInstance };
