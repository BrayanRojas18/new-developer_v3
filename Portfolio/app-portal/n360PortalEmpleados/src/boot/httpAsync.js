import catchErrors from "./catchErrors";
import { axiosInstance } from "./axios";
import env from "./env";

var loading = false;

export default function (url, config = {}) {
  const http = async (method, data, params = {}) => {
    return new Promise(async (resolve, reject) => {
      const processResult = (result) => {
        resolve(result.data);
      };

      const processErrors = (error) => {
        catchErrors(error);
        reject(null);
      };

      const request = {
        method: method,
        url: env.apiUrl + url,
        params: params,
        data: data,
      };

      try {
        const result = await axiosInstance(request);
        processResult(result);
      } catch (error) {
        processErrors(error);
      }
    });
  };

  return {
    get: async (data) => {
      return await http("get", null, data);
    },
    post: async (data, params) => {
      return await http("post", data, params);
    },
    put: async (data, params) => {
      return await http("put", data, params);
    },
    delete: async function (data, params) {
      return await http("delete", data, params);
    },
  };
}
