import _ from "lodash";
import auth from "./auth";
import Vue from "vue";

export default function (error) {
  const bus = new Vue({});

  if (error.response) {
    if (typeof error.response.data == "string") {
      auth.logout();
      window.location.href = "/#/login";
      bus.$q.notify(
        error.response.data.length > 500
          ? "Algo ha salido mal..."
          : error.response.data
      );
    } else {
      _.forEach(error.response.data, (val, key) => {
        var msg = val;
        if (typeof val != "string") msg = val[0];
        bus.$q.notify(msg);
      });
    }
  } else if (error.request) {
    bus.$q.notify("Error");
    auth.logout();
    window.location.href = "/#/login";
  } else if (error.message != "manual_cancel") {
    bus.$q.notify(error.message);
  }
}
