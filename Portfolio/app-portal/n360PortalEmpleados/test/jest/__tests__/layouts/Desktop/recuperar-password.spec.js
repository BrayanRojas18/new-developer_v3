import { mount, createLocalVue, shallowMount } from "@vue/test-utils";
import RecoverPass from "src/layouts/Desktop/Login/recuperar-password.vue";
import * as All from "quasar";
const { Quasar } = All;
import { axiosInstance } from "src/boot/axios";

const components = Object.keys(All).reduce((object, key) => {
  const val = All[key];
  if (val && val.component && val.component.name != null) {
    object[key] = val;
  }
  return object;
}, {});

describe("RecoverPass (LOGIN)", () => {
  const localVue = createLocalVue();
  localVue.use(Quasar, { components });
  const api = jest.spyOn(axiosInstance, "post");

  it("Datos predetermindados(data()) correctos", () => {
    const wrapper = mount(RecoverPass, {
      localVue,
    });

    const vm = wrapper.vm;

    expect(typeof vm.form).toBe("object");
    expect(typeof vm.form.email).toBe("string");
    expect(typeof vm.mostrarMensajeRecuperacion).toBe("boolean");
    expect(typeof vm.loadingRecuperacionPassword).toBe("boolean");
    expect(vm.form.email).toBe("");
    expect(vm.mostrarMensajeRecuperacion).toBe(false);
    expect(vm.loadingRecuperacionPassword).toBe(false);
  });
  it("Formularios con email vacío (método recuperarPassword)", async () => {
    const email = "";
    const $q = {
      notify: function () {
        return {
          message: "Campos [Email] requerido",
          color: "negative",
        };
      },
    };

    const wrapper = shallowMount(RecoverPass, {
      localVue,
      mocks: {
        $api: axiosInstance,
        $q,
      },
    });
    const vm = wrapper.vm;

    const error = {
      error: "Campos [Email] requerido",
      status: 403,
    };
    api.mockImplementation(() => new Promise.reject(error));
    await expect(vm.recuperarPassword(email)).rejects.toEqual(error);
    expect(axiosInstance.post).toHaveBeenCalledTimes(1);
  });
});
