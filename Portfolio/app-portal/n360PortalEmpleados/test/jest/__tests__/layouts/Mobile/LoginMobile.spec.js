import { mount, createLocalVue, shallowMount } from "@vue/test-utils";
import LoginMobile from "src/layouts/Mobile/Login/LoginMobile.vue";
import * as All from "quasar";
const { Quasar } = All;
import { axiosInstance } from "src/boot/axios";

const components = Object.keys(All).reduce((object, key) => {
  const val = All[key];
  if (val && val.component && val.component.name != null) {
    object[key] = val;
  }
  return object;
}, {});

describe("LoginMobile (LOGIN)", () => {
  const localVue = createLocalVue();
  localVue.use(Quasar, { components });

  it("Datos predetermindados(data()) correctos", () => {
    const $q = {
      localStorage: {
        has: function () {},
      },
    };
    const wrapper = shallowMount(LoginMobile, {
      localVue,
      mocks: {
        $q,
      },
    });

    const vm = wrapper.vm;

    expect(typeof vm.form).toBe("object");
    expect(typeof vm.form.email).toBe("string");
    expect(typeof vm.form.password).toBe("string");
    expect(typeof vm.windowHeight).toBe("string");
    expect(typeof vm.textoBienvenida).toBe("string");
    expect(typeof vm.recoverPassword).toBe("boolean");
    expect(vm.form.email).toBe("");
    expect(vm.form.password).toBe("");
    expect(vm.recoverPassword).toBe(false);
  });
  it("Btn ¿Olvidaste tu contraseña? para abrir dialogo", async () => {
    const $q = {
      localStorage: {
        has: function () {},
      },
    };
    const wrapper = shallowMount(LoginMobile, {
      localVue,
      mocks: {
        $q,
      },
    });
    const vm = wrapper.vm;

    const button = wrapper.findComponent({ ref: "btnRecoverPassword" });
    await button.trigger("click");
    expect(vm.recoverPassword).toBe(false);
  });
  describe("Login", () => {
    const api = jest.spyOn(axiosInstance, "post");
    it("Formularios vacíos (método callSession)", async () => {
      const email = "";
      const password = "";
      const $q = {
        localStorage: {
          has: function () {},
        },
      };

      const wrapper = shallowMount(LoginMobile, {
        localVue,
        mocks: {
          $api: axiosInstance,
          $q,
        },
      });
      const vm = wrapper.vm;

      const error = {
        error: "Campos [Email & Contraseña] requeridos",
        status: 403,
      };
      api.mockImplementation(() => new Promise.reject(error));
      await expect(vm.callSession(email, password)).rejects.toEqual(error);
      expect(axiosInstance.post).toHaveBeenCalledTimes(1);
    });
    it("Formularios con datos incorrectos (método callSession)", async () => {
      const email = "employee@prueba.com";
      const password = "null";
      const $q = {
        localStorage: {
          has: function () {},
        },
      };

      const wrapper = shallowMount(LoginMobile, {
        localVue,
        mocks: {
          $api: axiosInstance,
          $q,
        },
      });
      const vm = wrapper.vm;

      const error = {
        error: "Usuario no encontrado",
        status: 401,
      };
      api.mockImplementation(() => new Promise.reject(error));
      await expect(vm.callSession(email, password)).rejects.toEqual(error);
      expect(axiosInstance.post).toHaveBeenCalledTimes(2);
    });
    it("Formularios con datos correctos (método callSession)", async () => {
      const email = "employee@prueba.com";
      const password = "123456";
      const $q = {
        localStorage: {
          has: function () {},
        },
      };

      const wrapper = shallowMount(LoginMobile, {
        localVue,
        mocks: {
          $api: axiosInstance,
          $q,
        },
      });
      const vm = wrapper.vm;

      const response = {
        data: {},
        status: 200,
      };

      api.mockImplementation(() => new Promise.resolve(response));
      await expect(vm.callSession(email, password)).resolves.toEqual(
        response.data
      );
      expect(axiosInstance.post).toHaveBeenCalledTimes(3);
    });
    it("Método login", async () => {
      const email = "employee@prueba.com";
      const password = "123456";

      const error =
        "Existe un error con el usuario! Por favor contacte a soporte";
      const $q = {
        notify: function () {
          return {
            message: error,
            color: "negative",
          };
        },
        localStorage: {
          has: function () {},
        },
      };

      const notify = $q.notify();

      const wrapper = shallowMount(LoginMobile, {
        localVue,
        mocks: {
          $api: axiosInstance,
          $q,
        },
      });
      const vm = wrapper.vm;
      api.mockImplementation(() => new Promise.reject(error));
      await expect(vm.login(email, password)).resolves.toEqual(notify);
      expect(axiosInstance.post).toHaveBeenCalledTimes(4);
    });
    it("Método keepLogin", async () => {
      const $q = {
        localStorage: {
          has: function () {},
        },
      };

      const wrapper = shallowMount(LoginMobile, {
        localVue,
        mocks: {
          $q,
        },
      });
      const vm = wrapper.vm;
      await expect(vm.keepLogin()).resolves.toEqual(false);
    });
  });
});
