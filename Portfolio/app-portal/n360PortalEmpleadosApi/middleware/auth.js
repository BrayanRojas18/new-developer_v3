const jwt = require("jsonwebtoken");

const config = require("../env");

module.exports = {
  verifyToken: (req, res, next) => {
    const token = req.headers["access-token"];
  
    if (!token) {
      return res.status(401).json({
        error: "Token requerido",
        status: 401,
      });
    }
    try {
      const decoded = jwt.verify(token, config.key);
      req.user = decoded;
      next();
    } catch (err) {
      return res.status(401).json({
        error: "Token Invalido",
        status: 401,
      });
    }
  }
}
