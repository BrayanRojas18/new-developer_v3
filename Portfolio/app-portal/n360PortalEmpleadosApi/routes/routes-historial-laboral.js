const HistorialLaboralController = require("../controllers/HistorialLaboralController");
const router = require("express").Router();
const {verifyToken} = require("../middleware/auth");

// CONSULTAS
router.post("/historial", verifyToken, async function (req, res) {
  await HistorialLaboralController.Historial(req, res);
});

module.exports = router;
