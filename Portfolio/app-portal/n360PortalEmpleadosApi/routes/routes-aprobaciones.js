const AprobacionesController = require("../controllers/AprobacionesController");
const router = require("express").Router();
const {verifyToken} = require("../middleware/auth");

// APROBAR & RECHAZAR
router.post("/aprobar", verifyToken, async function (req, res) {
  await AprobacionesController.Aprobar(req, res);
});
router.post("/rechazar", verifyToken, async function (req, res) {
  await AprobacionesController.Rechazar(req, res);
});

// CONSULTAS

router.post("/solicitudes", verifyToken, async function (req, res) {
  await AprobacionesController.Solicitudes(req, res);
});

module.exports = router;
