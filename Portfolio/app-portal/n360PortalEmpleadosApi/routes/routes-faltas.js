const FaltasController = require("../controllers/FaltasController");
const router = require("express").Router();
const {verifyToken} = require("../middleware/auth");

// CONSULTAS
router.post("/listado", verifyToken, async function (req, res) {
  await FaltasController.Listado(req, res);
});
router.post("/motivos-faltas", verifyToken, async function (req, res) {
  await FaltasController.MotivosFaltas(req, res);
});

module.exports = router;
