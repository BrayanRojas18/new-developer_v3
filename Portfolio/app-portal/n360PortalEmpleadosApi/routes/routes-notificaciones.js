const NotificacionesController = require("../controllers/NotificacionesController");
const router = require("express").Router();
const {verifyToken} = require("../middleware/auth");

router.post("/listado", verifyToken, async function (req, res) {
  await NotificacionesController.Listado(req, res);
});
router.post("/open", verifyToken, async function (req, res) {
  await NotificacionesController.Open(req, res);
});
router.post("/get-doc", verifyToken, async function (req, res) {
  await NotificacionesController.GetDoc(req, res);
});
router.post("/configuracion", verifyToken, async function (req, res) {
  await NotificacionesController.Configuracion(req, res);
});
router.post("/get-configuracion", verifyToken, async function (req, res) {
  await NotificacionesController.GetConfig(req, res);
});

module.exports = router;
