const CapacitacionesController = require("../controllers/CapacitacionesController");
const router = require("express").Router();
const {verifyToken} = require("../middleware/auth");

// CONSULTAS
router.post("/listado", verifyToken, async function (req, res) {
  await CapacitacionesController.Listado(req, res);
});

module.exports = router;
