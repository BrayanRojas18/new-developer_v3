const RubrosController = require("../controllers/RubrosController");
const router = require("express").Router();
const {verifyToken} = require("../middleware/auth");

// CONSULTAS

router.post("/rubros-ingreso", verifyToken, async function (req, res) {
  await RubrosController.RubrosIngreso(req, res);
});

router.post("/rubros-egreso", verifyToken, async function (req, res) {
    await RubrosController.RubrosEgreso(req, res);
});
  
router.post("/rubros-provisiones", verifyToken, async function (req, res) {
    await RubrosController.RubrosProvisiones(req, res);
});

module.exports = router;
