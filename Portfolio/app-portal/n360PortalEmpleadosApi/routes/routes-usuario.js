const UsersController = require("../controllers/UsersController");
const router = require("express").Router();
const { body, validationResult } = require("express-validator");
const {verifyToken} = require("../middleware/auth");

router.post("/signup", body("email").isEmail(), async function (req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res
      .status(400)
      .json({ error: "El correo ingresado no es un correo", status: 400 });
  }
  await UsersController.signUp(req, res);
});
router.post("/login", body("email").isEmail(), async function (req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res
      .status(400)
      .json({ error: "El correo ingresado no es un correo", status: 400 });
  }
  await UsersController.login(req, res);
});
// router.post('/recover-password', async function (req, res) {
// 	let conf = {}
// 	const { email } = req.body
// 	if (!email)
// 		res.status(403).json({
// 			error: 'Campos [Email] requerido',
// 			tatus: 403,
// 		})

// 	const instance = new DB(req, res, conf)
// 	try {
// 		const val = await instance.getFunction('auth/recover/index', { email }, req.headers)

// 		res.status(200).json({
// 			result: true,
// 			data: val,
// 		})
// 	} catch (error) {
// 		res.status(500).json({
// 			error: 'Internal server error',
// 		})
// 	}
// })
router.post("/home", verifyToken, async function (req, res) {
  await UsersController.home(req, res);
});

module.exports = router;
