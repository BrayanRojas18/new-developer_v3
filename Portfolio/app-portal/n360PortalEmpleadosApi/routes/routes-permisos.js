const PermisosController = require("../controllers/PermisosController");
const router = require("express").Router();
const {verifyToken} = require("../middleware/auth");

// Solicitar
router.post("/solicitar", verifyToken, async function (req, res) {
  await PermisosController.Solicitar(req, res);
});

// VALIDACIONES & CONSULTAS
router.post("/motivos-permisos-medicos", verifyToken, async function (req, res) {
  await PermisosController.MotivosPermisosMedicos(req, res);
});
router.post("/motivos-permisos-otros", verifyToken, async function (req, res) {
  await PermisosController.MotivosPermisosOtros(req, res);
});
router.post("/maternidad-paternidad", verifyToken, async function (req, res) {
  await PermisosController.MaternidadPaternidad(req, res);
});
router.post("/mi-aprobador", verifyToken, async function (req, res) {
  await PermisosController.MiAprobador(req, res);
});
router.post("/solicitudes", verifyToken, async function (req, res) {
  await PermisosController.Solicitudes(req, res);
});
router.post("/activos-pagados", verifyToken, async function (req, res) {
  await PermisosController.Activos(req, res);
});
router.post("/historico", verifyToken, async function (req, res) {
  await PermisosController.Historico(req, res);
});
router.post("/get-permiso", verifyToken, async function (req, res) {
  await PermisosController.GetOne(req, res);
});
router.post("/eliminar", verifyToken, async function (req, res) {
  await PermisosController.Eliminar(req, res);
});
router.post("/upload-file", verifyToken, async function (req, res) {
  await PermisosController.UploadFile(req, res);
});
router.post("/remove-file", verifyToken, async function (req, res) {
  await PermisosController.RemoveFile(req, res);
});
router.post("/ultimo-rol", verifyToken, async function (req, res) {
  await PermisosController.UltimoRol(req, res);
});

module.exports = router;
