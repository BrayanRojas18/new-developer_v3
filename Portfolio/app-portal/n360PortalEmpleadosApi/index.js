const env = require("./env");
const express = require("express");
const app = express();
var http = require("http").Server(app);
const cors = require("cors");
const bodyParser = require("body-parser");
const { connectDB } = require("./db/mongoose");
const { Server } = require("socket.io");
const io = new Server(http, {
  cors: {
    origin: env.urls,
    methods: ["GET", "POST"],
  },
});

connectDB(process.env.mongodbUrl || "");

const corsOptions = (req, callback) => {
  let cors;
  if (
    env.urls.some((v) => v == req.header("Origin")) ||
    req.header("api-token") === env.apiToken
  ) {
    cors = { origin: true };
  } else {
    cors = { origin: false };
  }
  callback(null, cors);
};

app.options("*", cors());
app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use("/api", require("./routes"));
app.use("/api", express.static("files"));

const port = process.env.PORT || env.port;
io.on("connection", (socket) => {
  console.log("a user connected");

  socket.on("notify", (req) => {
    io.emit("notify", "lista");
  });

  socket.on("connect_error", (err) => {
    console.log(`connect_error due to ${err.message}`);
  });
});

http.listen(port, function () {
  console.log("Server listening on port: " + port);

  console.log("--------------------------------------------------");
});
