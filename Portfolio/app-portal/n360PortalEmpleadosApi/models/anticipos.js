const mongoose = require("mongoose");
const { Schema } = mongoose;
const moment = require("moment");

const anticiposSchema = new Schema({
  employee: {
    type: String,
    required: true,
  },
  amount: {
    type: Number,
    required: true,
  },
  date: {
    type: String,
    required: true,
  },
  pagar_beneficio: {
    type: Boolean,
  },
  beneficio: {
    type: String,
  },
  year: {
    type: Number,
  },
  month: {
    type: Number,
  },
  rol: {
    type: String,
  },
  motivoAnticipo: {
    type: String,
  },
  estatus: {
    type: String,
    required: true,
    default: "p",
  },
  aprobador: {
    type: String,
  },
  file: {
    type: String,
  },
  site: {
    type: String,
    default: "portal",
  },
  anticipo_id: {
    type: String,
  },
  __company__: {
    type: mongoose.ObjectId,
  },
  created_at: {
    type: Date,
    default: moment(),
  },
});

module.exports = mongoose.model("anticipos", anticiposSchema);
