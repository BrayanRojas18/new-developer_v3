const mongoose = require("mongoose");
const { Schema } = mongoose;
const moment = require("moment");

const notificacionesSchema = new Schema({
  title: {
    type: String,
  },
  content: {
    type: String,
  },
  employee: {
    type: String,
    required: true,
  },
  solicitud: {
    type: String,
  },
  open: {
    type: Boolean,
    default: false,
  },
  action: {
    type: String,
  },
  type: {
    type: String,
  },
  site: {
    type: String,
  },
  __company__: {
    type: mongoose.ObjectId,
  },
  created_at: {
    type: Date,
    default: moment(),
  },
});

module.exports = mongoose.model("notificaciones", notificacionesSchema);
