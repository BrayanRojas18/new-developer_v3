const mongoose = require("mongoose");
const { Schema } = mongoose;
const moment = require("moment");

const companiesSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  active: {
    type: Boolean,
    required: true,
  },
  admin: {
    type: mongoose.ObjectId,
    required: true,
  },
  apiUrl: {
    type: String,
    required: true,
  },
  created_at: {
    type: Date,
    default: moment(),
  },
});

module.exports = mongoose.model("companies", companiesSchema);
