const mongoose = require("mongoose");
const { Schema } = mongoose;
const moment = require("moment");

const userSchema = new Schema({
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  empleado_id: {
    type: mongoose.ObjectId,
  },
  active: {
    type: Boolean,
    required: true,
  },
  role: {
    type: String,
    required: true,
  },
  notifications: {
    type: Boolean,
    default: true,
  },
  __company__: {
    type: mongoose.ObjectId,
  },
  created_at: {
    type: Date,
    default: moment(),
  },
});

module.exports = mongoose.model("usuarios", userSchema);
