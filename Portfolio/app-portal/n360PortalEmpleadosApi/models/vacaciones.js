const mongoose = require("mongoose");
const { Schema } = mongoose;
const moment = require("moment");

const vacacionesSchema = new Schema({
  employee: {
    type: String,
    required: true,
  },
  start: {
    type: String,
    required: true,
  },
  end: {
    type: String,
  },
  days: {
    type: Number,
    required: true,
  },
  days_adicionales: {
    type: Number,
    default: 0,
  },
  period: {
    type: Number,
    required: true,
  },
  periodo: {
    type: String,
    required: true,
  },
  valor: {
    type: Number,
    required: true,
  },
  valor_adicional: {
    type: Number,
  },
  mode: {
    type: String,
    required: true,
    default: "norm",
  },
  mode_adicional: {
    type: String,
  },
  estatus: {
    type: String,
    required: true,
    default: "p",
  },
  aprobador: {
    type: String,
  },
  file: {
    type: String,
  },
  site: {
    type: String,
    default: "portal",
  },
  vacaciones_id: {
    type: String,
  },
  __company__: {
    type: mongoose.ObjectId,
  },
  created_at: {
    type: Date,
    default: moment(),
  },
});

module.exports = mongoose.model("vacaciones", vacacionesSchema);
