const { Request, Response } = require("express");
const { api } = require("../db/api.js");
const { upload, remove } = require("../storage/storage.js");
const Prestamos = require("../models/prestamos.js");
const moment = require("moment");
const multer = require("multer");
const fs = require("fs");
const { MailAprobar } = require("./NotificacionesController.js");

const validar_rol = async (url, company, params, token) => {
  let roles = await api("roles-tipos/roles", url, "GET", {
    conditions: {
      __company__: company,
    },
  }, token);
  roles = roles.data.length ? roles.data : [];

  const index = await api("roles/roles", url, "GET", {
    conditions: {
      year: moment(params.date).year(),
      month: Number(moment(params.date).format("M")),
      rol: roles.find((v) => v.id == "fdm")._id,
      __company__: company,
    },
  }, token);
  if (index.data.length) return true;
  else return false;
};
const find_apro_dpt = async (url, user, token) => {
  let contrato = await api("usuarios/contratos", url, "GET", {
    conditions: {
      empleado: user,
      $or: [{ liquidado: { $exists: false } }, { liquidado: false }],
    },
  });
  contrato = contrato.data.length ? contrato.data[0] : null;
  if (contrato) {
    let dpt = await api("departamentos/departamentos", url, "GET", {
      conditions: {
        _id: contrato.departamento,
      },
    }, token);
    let usuario = null;
    if (dpt.data.length) {
      usuario = await api("usuarios/usuarios", url, "GET", {
        conditions: {
          _id: dpt.data[0].approve_prestamos,
        },
      });
      usuario = usuario.data.length ? usuario.data[0] : null;
    }

    return {
      exist: dpt.data.length && dpt.data[0].approve_prestamos ? true : false,
      apro: usuario ? usuario.name + " " + usuario.last_name : "Sin aprobador",
    };
  } else return { exist: false, apro: null };
};
const getCollection = async (path) => {
  return (path || "").split("/")[0];
};
const getDoc = async (path) => {
  return (path || "").split("/")[1];
};
const saveFile = async (data, company, user, id) => {
  const file = `${data.file.split("/")[0]}/${id} - ${data.file.split("/")[1]}`;

  const comprobante = await upload({
    filePath: `./files/${company}/${user.data._id}/${data.file}`,
    destFileName: `files/${company}/${user.data._id}/${file}`,
  });

  // ELIMNAR ARCHIVO DEL API
  fs.access(`files/${company}/${user.data._id}/${data.file}`, (error) => {
    if (!error) {
      fs.unlinkSync(`files/${company}/${user.data._id}/${data.file}`);
    } else {
      console.error("Error occured:", error);
    }
  });

  return comprobante[0].name;
};

module.exports = {
  Solicitar: async (request = Request, response = Response) => {
    try {
      const { user, company, parent_company, url, data, file } = request.body;

      // VALIDAR SI HAY ROL GENERADO SEGUN LA FECHA DE SOLICITUD
      const g_rol = await validar_rol(url, company, data, request.headers["access-token"]);
      if (g_rol)
        return response.status(403).json({
          error:
            "Hay rol generado según la fecha del prestamo, por lo tanto no se puede generar la solicitud. ",
          status: 403,
        });

      // BUSCAR APROBADOR DE DEPARTAMENTO
      const apro_dpt = await find_apro_dpt(url, user.data._id, request.headers["access-token"]);

      let form = {
        employee: data.employee,
        motivo: data.motivo,
        amount: data.amount,
        date: data.date,
        dues: data.dues,
        cuotas: data.cuotas,
        tipo_prestamo: data.tipo_prestamo,
        ingreso: data.ingreso,
        site: "portal",
        estatus: "p",
        __company__: company,
      };

      let doc;
      if (apro_dpt.exist) {
        if (data._id) {
          if (data.file && file) {
            form["file"] = await saveFile(data, company, user, data._id);
          }
          await Prestamos.updateOne({ _id: data._id }, form);
        } else {
          doc = await Prestamos.create(form);

          // CARGAR ARCHIVO SI ES QUE HAY
          if (data.file && file) {
            const comprobante = await saveFile(data, company, user, doc._id);

            await Prestamos.updateOne({ _id: doc._id }, { file: comprobante });
          }
        }

        // SEND NOTIFICATION
        await MailAprobar(user, company, parent_company, url, "prestamo");
      } else {
        if (data._id) {
          form._id = data._id;
          if (data.file && file) {
            form["file"] = await saveFile(data, company, user, data._id);
          }
        }
        form.employee = user.data._id;
        const res = await api("prestamos/solicitar", url, "POST", {
          config: { company, parent_company },
          form,
        }, request.headers["access-token"]);

        if (!data._id) {
          if (data.file && file) {
            form["file"] = await saveFile(data, company, user, res.data._id);
            form._id = res.data._id;
            await api("prestamos/solicitar", url, "POST", {
              config: { company, parent_company },
              form,
            }, request.headers["access-token"]);
          }
        }
      }

      return response.status(200).json({
        result: true,
        data: true,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },

  TipoSueldo: async (request = Request, response = Response) => {
    try {
      const { user, url, company } = request.body;

      let tipos_sueldo = await api("prestamos/tipo-sueldo", url, "POST", {
        user,
        config: { company: company },
      }, request.headers["access-token"]);
      return response.status(200).json({
        result: true,
        data: tipos_sueldo.data,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  ConfiguracionPrestamos: async (request = Request, response = Response) => {
    try {
      const { url, company } = request.body;

      let configuracion = await api(
        "prestamos/configuracion-prestamos",
        url,
        "POST",
        {
          config: { company: company },
          conditions: {
            _id: "prestamos",
          },
        }, request.headers["access-token"]
      );
      configuracion = configuracion.data.length ? configuracion.data[0] : null;
      return response.status(200).json({
        result: true,
        data: configuracion,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  MontoMaximo: async (request = Request, response = Response) => {
    try {
      const { user, url, company } = request.body;

      let maximo = await api("prestamos/monto-maximo", url, "POST", {
        user,
        config: { company: company },
      }, request.headers["access-token"]);
      maximo = maximo.data ? maximo.data : 0;
      return response.status(200).json({
        result: true,
        data: maximo,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  MiAprobador: async (request = Request, response = Response) => {
    try {
      const { user, url, company } = request.body;

      let aprobador = await api("prestamos/mi-aprobador", url, "POST", {
        user,
        config: { company: company },
      }, request.headers["access-token"]);
      return response.status(200).json({
        result: true,
        data: aprobador.data,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  Solicitudes: async (request = Request, response = Response) => {
    try {
      const { user, company, url, type } = request.body;

      // Pentientes o Rechazadas por Aprobador de DPT
      let prestamos_dpt = await Prestamos.find({
        employee: user._id,
        $or: [{ estatus: "p" }, { estatus: "r" }],
        __company__: company,
      });

      //BUSCAR PRESTAMOS SOLICITADOS EN EL API EXTERNO
      const solicitudes = await api("prestamos/solicitudes", url, "POST", {
        config: { company },
        data: { user: user.data._id, type, prestamos_dpt: prestamos_dpt },
      }, request.headers["access-token"]);

      let data = solicitudes.data.sort(
        (a, b) =>
          moment(b.date, "DD-MM").unix() - moment(a.date, "DD-MM").unix()
      );
      data = data.map(v => {
        return {
          ...v,
          date: moment(v.date).format('YYYY-MM-DD'),
        }
      })

      return response.status(200).json({
        result: true,
        data: data,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  Activos: async (request = Request, response = Response) => {
    try {
      const { user, company, url, type } = request.body;

      //BUSCAR PRESTAMOS ACTIVOS
      const activos = await api("prestamos/activos-pagados", url, "POST", {
        config: { company },
        data: { user: user.data._id, type },
      }, request.headers["access-token"]);

      const data = [...activos.data];
      return response.status(200).json({
        result: true,
        data: data,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  Historico: async (request = Request, response = Response) => {
    try {
      const { user, id, company, url } = request.body;

      // Pentientes o Rechazadas por Aprobador de DPT
      let prestamos_dpt = await Prestamos.find({
        prestamo_id: id,
      });
      const historico = await api("prestamos/historico", url, "POST", {
        config: { company },
        data: { user: user.data._id, id, prestamos_dpt: prestamos_dpt },
      }, request.headers["access-token"]);

      return response.status(200).json({
        result: true,
        data: historico.data,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  Eliminar: async (request = Request, response = Response) => {
    try {
      const { collection, company, parent_company, url, file } = request.body;
      const path = await getCollection(collection);

      if (path === "prestamos") {
        const doc = await getDoc(collection);
        await Prestamos.deleteOne({ _id: doc });
      } else {
        await api("prestamos/eliminar", url, "POST", {
          config: { company, parent_company },
          collection,
        }, request.headers["access-token"]);
      }
      if (file) {
        await remove({
          fileName: file,
        });
      }
      return response.status(200).json({
        result: true,
        data: true,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  EstadoCuentaPdf: async (request = Request, response = Response) => {
    try {
      const { data, company, url } = request.body;

      const pdf = await api("prestamos/estado-pdf", url, "POST", {
        config: { company },
        data: data,
      }, request.headers["access-token"]);

      return response.status(200).json({
        result: true,
        data: pdf.data,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  GetOne: async (request = Request, response = Response) => {
    try {
      const { collection, company, parent_company, url } = request.body;
      const path = await getCollection(collection);

      let prestamo = null;
      if (path === "prestamos") {
        const doc = await getDoc(collection);
        prestamo = await Prestamos.findById(doc).exec();
      } else {
        prestamo = await api("prestamos/get-prestamo", url, "POST", {
          config: { company, parent_company },
          collection,
        }, request.headers["access-token"]);
        prestamo = prestamo.data ? prestamo.data : null;
      }

      if(prestamo) {
        prestamo.date = moment(prestamo.date).format('YYYY-MM-DD')
      }

      return response.status(200).json({
        result: true,
        data: prestamo,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  UploadFile: async (request = Request, response = Response) => {
    const req = request;
    const res = response;
    try {
      let uploadedFileName = "";
      const storage = multer.diskStorage({
        destination: async function (req, file, cb) {
          let dirPath = `files/${req.query.company}/${req.query.user}/comprobantes-prestamos`;
          if (!fs.existsSync(dirPath)) {
            await fs.mkdirSync(dirPath, { recursive: true });
          }
          cb(null, dirPath + "/");
        },
        filename: async function (req, file, cb) {
          const fileName = file.originalname;
          uploadedFileName = fileName;
          cb(null, fileName);
        },
      });

      let upload_ = multer({
        storage: storage,
      }).array("file", 12);
      upload_(req, res, function (err) {
        if (err) {
          return res.status(403).json({
            error: "Error al cargar archivo. ",
            status: 403,
          });
        } else {
          return res.status(200).json({
            result: true,
            data: uploadedFileName,
          });
        }
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  RemoveFile: async (request = Request, response = Response) => {
    try {
      const { file, collection, company, parent_company, url, data } =
        request.body;

      const path = await getCollection(collection);
      const doc = await getDoc(collection);

      // VALIDAR SI HAY ROL GENERADO SEGUN LA FECHA DE SOLICITUD
      const g_rol = await validar_rol(url, company, data, request.headers["access-token"]);
      if (g_rol)
        return response.status(403).json({
          error:
            "Hay rol generado según la fecha del préstamo, por lo tanto no se puede realizar algún cambio. ",
          status: 403,
        });

      if (path === "prestamos") {
        await Prestamos.updateOne({ _id: doc }, { file: null });
      } else {
        await api("prestamos/editar", url, "POST", {
          config: { company, parent_company },
          collection,
          form: { file: null },
        }, request.headers["access-token"]);
      }

      await remove({
        fileName: file,
      });
      return response.status(200).json({
        result: true,
        data: true,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
};
