const { Request, Response } = require("express");
const { api } = require("../db/api");
const moment = require("moment");

const find_dpts = async (url, company, token) => {
  const dpts = await api("departamentos/departamentos", url, "GET", {
    conditions: {
      __company__: company,
    },
  }, token);
  return dpts.data;
};

module.exports = {
  Historial: async (request = Request, response = Response) => {
    try {
      const { user, company, parent_company, url } = request.body;
      const dpts = await find_dpts(url, company, request.headers["access-token"]);

      // CONTRATOS
      let contratos = await api("usuarios/contratos", url, "GET", {
        conditions: {
          empleado: user,
        },
      });
      contratos = contratos.data.map((v) => {
        let dpt;
        if (dpts.length) dpt = dpts.find((d) => d._id == v.departamento);
        dpt = dpt ? dpt.name : "Sin nombre";
        return {
          ...v,
          departamento: dpt,
          entry_date: moment(v.entry_date).format("YYYY-MM-DD"),
          estatus: v.liquidado ? "INACTIVO" : "ACTIVO",
        };
      });

      // SUELDOS
      let sueldos = await api("usuarios/sueldos", url, "GET", {
        conditions: {
          empleado: user,
          tipo: "sueldo",
        },
      }, request.headers["access-token"]);

      //  VACACIONES
      let vacaciones = await api("vacaciones/vacaciones", url, "POST", {
        config: { company },
        employee: user,
      }, request.headers["access-token"]);

      //  ANTICIPOS
      let anticipos = await api("anticipos/anticipos", url, "POST", {
        config: { company },
        employee: user,
      }, request.headers["access-token"]);

      //  PRESTAMOS
      let prestamos = await api("prestamos/prestamos", url, "POST", {
        config: { company },
        employee: user,
      }, request.headers["access-token"]);

      //  PERMISOS
      let permisos = await api("permisos/permisos", url, "POST", {
        config: { company },
        empleado: user,
      }, request.headers["access-token"]);

      //  LLAMADOS ATENCION
      let llamados = await api("llamados/listado", url, "GET", {
        conditions: {
          employee: user,
        },
      }, request.headers["access-token"]);

      //  CAPACITACIONES
      const caps = await api("capacitaciones/listado", url, "GET", {
        conditions: { __company__: company },
      }, request.headers["access-token"]);
      let capacitaciones = [];
      if (caps.data.length) {
        caps.data.forEach((res) => {
          let find = res.employee.find((v) => v.contrato.empleado == user);
          if (find) capacitaciones.push(res);
        });
      }

      //  FALTAS
      let faltas = await api("faltas/listado", url, "GET", {
        conditions: {
          employee: user,
        },
      }, request.headers["access-token"]);

      let data = {
        contratos: contratos,
        sueldos: sueldos.data,
        vacaciones: vacaciones.data,
        anticipos: anticipos.data,
        prestamos: prestamos.data,
        permisos: permisos.data,
        llamados: llamados.data,
        capacitaciones,
        faltas: faltas.data,
      };
      return response.status(200).json({
        result: true,
        data: data,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
};
