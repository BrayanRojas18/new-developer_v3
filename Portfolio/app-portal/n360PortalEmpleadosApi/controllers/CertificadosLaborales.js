const { Request, Response } = require("express");
const { api } = require("../db/api");
const moment = require("moment");

module.exports = {
  // --------------------------------//
  Listado: async (request = Request, response = Response) => {
    try {
      const { company, parent_company, url } = request.body;
      //BUSCAR CERTIFICADOS LABORALES EN EL API EXTERNO
      let solicitudes = await api(
        "certificados-laborales/listado",
        url,
        "POST",
        {
          config: { company, parent_company },
        }, 
        request.headers["access-token"]
      );
      return response.status(200).json({
        result: true,
        data: solicitudes.data,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  Pdf: async (request = Request, response = Response) => {
    try {
      const { company, parent_company, url, user, formato } = request.body;
      //BUSCAR CERTIFICADOS LABORALES EN EL API EXTERNO
      const reporte = await api("certificados-laborales/pdf", url, "POST", {
        config: { company, parent_company },
        empleado: user,
        formato: formato,
      }, request.headers["access-token"]);
      return response.status(200).json({
        result: true,
        data: reporte.data,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
};
