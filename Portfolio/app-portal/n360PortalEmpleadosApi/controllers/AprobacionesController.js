const { Request, Response } = require("express");
const { api } = require("../db/api.js");
const Usuarios = require("../models/usuarios.js");
const Prestamos = require("../models/prestamos.js");
const Anticipos = require("../models/anticipos.js");
const Permisos = require("../models/permisos.js");
const Vacaciones = require("../models/vacaciones.js");
const moment = require("moment");
const { MailRechazar, Notify } = require("./NotificacionesController.js");

const validar_rol = async (url, company, params, token) => {
  let roles = await api("roles-tipos/roles", url, "GET", {
    conditions: {
      __company__: company,
    },
  }, token);
  roles = roles.data.length ? roles.data : [];

  const index = await api("roles/roles", url, "GET", {
    conditions: {
      year: moment(params.date).year(),
      month: Number(moment(params.date).format("M")),
      rol: roles.find((v) => v.id == "fdm")._id,
      __company__: company,
    },
  }, token);
  if (index.data.length) return true;
  else return false;
};
const find_apro_dpt = async (url, company, user, token) => {
  let approve = {
    prestamos: false,
    anticipos: false,
    permisos: false,
    vacaciones: false,
    dpts: [],
  };
  let dpt = await api("departamentos/departamentos", url, "GET", {
    conditions:{
      __company__: company
    }
  }, token)

  if(dpt.data.length) {
    const approve_prestamos = dpt.data.filter(v => v.hasOwnProperty('approve_prestamos') && v.approve_prestamos == user)
    if (approve_prestamos.length) approve.prestamos = true;

    const approve_anticipos = dpt.data.filter(v => v.hasOwnProperty('approve_anticipos') && v.approve_anticipos == user)
    if (approve_anticipos.length) approve.anticipos = true;

    const approve_permisos = dpt.data.filter(v => v.hasOwnProperty('approve_permisos') && v.approve_permisos == user)
    if (approve_permisos.length) approve.permisos = true;

    const approve_vacaciones = dpt.data.filter(v => v.hasOwnProperty('approve_vacaciones') && v.approve_vacaciones == user)
    if (approve_vacaciones.length) approve.vacaciones = true;

    let array = [...approve_prestamos, ...approve_anticipos, ...approve_permisos, ...approve_vacaciones]
    array.forEach(v => {
      approve.dpts.push(v._id)
    })
    approve.dpts = approve.dpts.filter((item,index)=>{
      return approve.dpts.indexOf(item) === index;
    })
  } 

  return approve;
};
const get_users_dpts = async (url, dpts, usuarios) => {
  let emps = [];
  const contratos = await api("usuarios/contratos", url, "GET", {
    conditions: {
      departamento: {
          $in: dpts
      },
      $or: [{ liquidado: { $exists: false } }, { liquidado: false }],
    },
  });
  if (contratos.data.length) {
    contratos.data.forEach((c) => {
      const find = usuarios.find(f => f.empleado_id == c.empleado)
      if(find)
        emps.push(find._id);
    });
  }
  return emps;
};

module.exports = {
  Aprobar: async (request = Request, response = Response) => {
    try {
      const { user, company, parent_company, url, data } = request.body;

      // VALIDAR SI HAY ROL GENERADO SEGUN LA FECHA DE SOLICITUD
      const g_rol = await validar_rol(url, company, data, request.headers["access-token"]);
      if (g_rol)
        return response.status(403).json({
          error:
            "Hay rol generado según la fecha de la solicitud, por lo tanto no se puede aprobar la solicitud. ",
          status: 403,
        });

      if (data.type_solicitud == "prestamos") {
        let form = {
          motivo: data.motivo,
          amount: data.amount,
          date: data.date,
          dues: data.dues,
          cuotas: data.cuotas,
          tipo_prestamo: data.tipo_prestamo,
          ingreso: data.ingreso,
          __company__: company,
        };

        const emp = await Usuarios.findById(data.employee).exec();
        form.employee = emp.empleado_id;
        const res = await api("prestamos/solicitar", url, "POST", {
          config: { company, parent_company },
          form,
        });

        await Prestamos.updateOne(
          { _id: data._id },
          { aprobador: user.data._id, estatus: "a", prestamo_id: res.data._id }
        );
      }

      if (data.type_solicitud == "anticipos") {
        let form = {
          motivoAnticipo: data.motivoAnticipo,
          amount: data.amount,
          date: data.date,
          pagar_beneficio: data.pagar_beneficio,
          beneficio: data.beneficio,
          year: data.year,
          month: data.month,
          rol: data.rol,
          __company__: company,
        };

        const emp = await Usuarios.findById(data.employee).exec();
        form.employee = emp.empleado_id;
        const res = await api("anticipos/solicitar", url, "POST", {
          config: { company, parent_company },
          form,
        }, request.headers["access-token"]);

        await Anticipos.updateOne(
          { _id: data._id },
          { aprobador: user.data._id, estatus: "a", anticipo_id: res.data._id }
        );
      }

      if (data.type_solicitud == "permisos") {
        let form = {
          solicitud: data.solicitud,
          motivo: data.motivo,
          inicio: moment(data.inicio).format(),
          fin: moment(data.fin).format(),
          dias: data.dias,
          estatus: "a",
          __company__: company,
        };

        const emp = await Usuarios.findById(data.employee).exec();
        form.employee = emp.empleado_id;
        const res = await api("permisos/solicitar", url, "POST", {
          config: { company, parent_company },
          form,
        }, request.headers["access-token"]);

        await Permisos.updateOne(
          { _id: data._id },
          { aprobador: user.data._id, estatus: "a", permiso_id: res.data._id }
        );
      }

      if (data.type_solicitud == "vacaciones") {
        let form = {
          ...data,
          valor: data.valor,
          valor_adicional: data.valor_adicional,
          end: data.end,
          estatus: "a",
          __company__: company,
        };
        const emp = await Usuarios.findById(data.employee).exec();

        // BUSCAR LISTADO DE VACACIONES
        let list = await api("vacaciones/get-vacaciones-list", url, "POST", {
          id: emp.empleado_id,
          config: { company: company },
        }, request.headers["access-token"]);
        list = list.data ? list.data : [];
        form.employee = emp.empleado_id;
        form["data"] = list;
        delete form.aprobador;
        delete form._id;
        delete form.employee_label;
        delete form.type_solicitud;
        const res = await api("vacaciones/solicitar", url, "POST", {
          config: { company, parent_company },
          form,
        }, request.headers["access-token"]);

        await Vacaciones.updateOne(
          { _id: data._id },
          {
            aprobador: user.data._id,
            estatus: "a",
            vacaciones_id: res.data._id,
          }
        );
      }

      // SEND NOTIFICATION
      await Notify(user, company, parent_company, url, data, "aprobar");

      return response.status(200).json({
        result: true,
        data: true,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  Rechazar: async (request = Request, response = Response) => {
    try {
      const { user, data, company, parent_company, url } = request.body;
      let type_solicitud = "";

      if (data.type_solicitud == "prestamos") {
        type_solicitud = "prestamo";
        let form = {
          estatus: "r",
          aprobador: user.data._id,
        };
        await Prestamos.updateOne({ _id: data._id }, form);
      }

      if (data.type_solicitud == "anticipos") {
        type_solicitud = "anticipo";
        let form = {
          estatus: "r",
          aprobador: user.data._id,
        };
        await Anticipos.updateOne({ _id: data._id }, form);
      }

      if (data.type_solicitud == "permisos") {
        type_solicitud = "permiso";
        let form = {
          estatus: "r",
          aprobador: user.data._id,
        };
        await Permisos.updateOne({ _id: data._id }, form);
      }

      if (data.type_solicitud == "vacaciones") {
        type_solicitud = "vacaciones";
        let form = {
          estatus: "r",
          aprobador: user.data._id,
        };
        await Vacaciones.updateOne({ _id: data._id }, form);
      }

      // SEND NOTIFICATION
      await MailRechazar(
        user,
        company,
        parent_company,
        url,
        data,
        type_solicitud
      );

      // SEND NOTIFICATION
      await Notify(user, company, parent_company, url, data, "rechazar");

      return response.status(200).json({
        result: true,
        data: true,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },

  Solicitudes: async (request = Request, response = Response) => {
    try {
      const { company, url, user } = request.body;

      const usuarios = await Usuarios.find();
      let usuarios_api = await api("usuarios/usuarios", url, "GET", {
        conditions: {
          __company__: company,
        },
      }, request.headers["access-token"]);
      usuarios_api = usuarios_api.data.length ? usuarios_api.data : [];

      // Buscar apro dpt
      const apro = await find_apro_dpt(url, company, user, request.headers["access-token"]);
      const emps_dpt = await get_users_dpts(url, apro.dpts, usuarios);

      // Prestamos pentientes por Aprobador de DPT
      let prestamos = [];
      if (apro.prestamos) {
        prestamos = await Prestamos.find({
          estatus: "p",
          employee: {$in: emps_dpt},
          __company__: company,
        });
        prestamos = prestamos.map((v) => {
          const find = usuarios.find((e) => e._id == v._doc.employee);
          const emp = usuarios_api.find((e) => e._id == find.empleado_id);
          return {
            ...v._doc,
            employee_label: emp
              ? emp.name + " " + emp.last_name
              : "Empleado no encontrado",
            type_solicitud: "prestamos",
            date: moment(v._doc.date).format('YYYY-MM-DD'),
          };
        });
      }

      // Anticipos pentientes por Aprobador de DPT
      let anticipos = [];
      if (apro.anticipos) {
        anticipos = await Anticipos.find({
          estatus: "p",
          employee: {$in: emps_dpt},
          __company__: company,
        });
        anticipos = anticipos.map((v) => {
          const find = usuarios.find((e) => e._id == v._doc.employee);
          const emp = usuarios_api.find((e) => e._id == find.empleado_id);
          return {
            ...v._doc,
            employee_label: emp
              ? emp.name + " " + emp.last_name
              : "Empleado no encontrado",
            type_solicitud: "anticipos",
            date: moment(v._doc.date).format('YYYY-MM-DD'),
          };
        });
      }
      // Permisos pentientes por Aprobador de DPT
      let permisos = [];
      if (apro.permisos) {
        permisos = await Permisos.find({
          estatus: "p",
          employee: {$in: emps_dpt},
          __company__: company,
        });
        permisos = permisos.map((v) => {
          const find = usuarios.find((e) => e._id == v._doc.employee);
          const emp = usuarios_api.find((e) => e._id == find.empleado_id);
          return {
            ...v._doc,
            employee_label: emp
              ? emp.name + " " + emp.last_name
              : "Empleado no encontrado",
            type_solicitud: "permisos",
            inicio: moment(v._doc.inicio).format('YYYY-MM-DD'),
            fin: moment(v._doc.fin).format('YYYY-MM-DD')
          };
        });
      }
      // Vacaciones pentientes por Aprobador de DPT
      let vacaciones = [];
      if (apro.vacaciones) {
        vacaciones = await Vacaciones.find({
          estatus: "p",
          employee: {$in: emps_dpt},
          __company__: company,
        });
        vacaciones = vacaciones.map((v) => {
          const find = usuarios.find((e) => e._id == v._doc.employee);
          const emp = usuarios_api.find((e) => e._id == find.empleado_id);
          return {
            ...v._doc,
            employee_label: emp
              ? emp.name + " " + emp.last_name
              : "Empleado no encontrado",
            type_solicitud: "vacaciones",
            start: moment(v._doc.start).format('YYYY-MM-DD'),
            end: moment(v._doc.end).format('YYYY-MM-DD')
          };
        });
      }

      const solicitudes = {
        prestamos: prestamos,
        anticipos: anticipos,
        permisos: permisos,
        vacaciones: vacaciones,
      };
      return response.status(200).json({
        result: true,
        data: solicitudes,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
};
