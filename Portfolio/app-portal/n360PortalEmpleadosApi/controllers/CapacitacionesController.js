const { Request, Response } = require("express");
const { api } = require("../db/api");
const moment = require("moment");

module.exports = {
  // --------------------------------//
  Listado: async (request = Request, response = Response) => {
    try {
      const { user, url } = request.body;
      //BUSCAR CAPACITACIONES GENERADAS EN EL API EXTERNO
      let capacitaciones = await api("capacitaciones/listado", url, "GET", {
        conditions: { __company__: user.data.__company__ },
      }, request.headers["access-token"]);
      let array = [];
      if (capacitaciones.data.length) {
        capacitaciones.data.forEach((res) => {
          let find = res.employee.find(
            (v) => v.contrato.empleado == user.data._id
          );
          if (find) array.push(res);
        });
      }
      const data = array.sort(
        (a, b) =>
          moment(b.created_at, "DD-MM").unix() -
          moment(a.created_at, "DD-MM").unix()
      );
      return response.status(200).json({
        result: true,
        data: data,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
};
