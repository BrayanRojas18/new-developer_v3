const { Request, Response } = require("express");
const { api } = require("../db/api");
const moment = require("moment");

module.exports = {
  // --------------------------------//
  RubrosIngreso: async (request = Request, response = Response) => {
    try {
      const {company, url } = request.body;

      let rubros = await api("rubros/rubros-ingreso", url, "GET", {
        conditions: {
          __company__: company,
        },
      }, request.headers["access-token"]);

      return response.status(200).json({
        result: true,
        data: rubros.data || [],
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  RubrosEgreso: async (request = Request, response = Response) => {
    try {
      const {company, url } = request.body;

      let rubros = await api("rubros/rubros-egreso", url, "GET", {
        conditions: {
          __company__: company,
        },
      }, request.headers["access-token"]);

      return response.status(200).json({
        result: true,
        data: rubros.data || [],
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  RubrosProvisiones: async (request = Request, response = Response) => {
    try {
      const {company, url } = request.body;

      let rubros = await api("rubros/rubros-provisiones", url, "GET", {
        conditions: {
          __company__: company,
        },
      }, request.headers["access-token"]);

      return response.status(200).json({
        result: true,
        data: rubros.data || [],
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
};
