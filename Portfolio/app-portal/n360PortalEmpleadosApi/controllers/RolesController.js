const { Request, Response } = require("express");
const { api } = require("../db/api");
const moment = require("moment");

const fecha_rol = async ({ url, year, month, rol }, token) => {
  let fecha = moment([year, month - 1]);

  let role = await api("roles-tipos/roles", url, "GET", {
    conditions: {
      _id: rol,
    },
  }, token);
  role = role.data.length ? role.data[0] : {};

  let dia_rol =
    role.generate_type === "primero"
      ? fecha.clone().startOf("month").date()
      : role.generate_type === "ultimo"
      ? fecha.clone().endOf("month").date()
      : role.generate_type === "dia_semana"
      ? rol.split("_")[1]
      : role.generate_day;
  fecha = moment([year, month - 1, dia_rol]);

  return fecha;
};
module.exports = {
  // --------------------------------//
  Listado: async (request = Request, response = Response) => {
    try {
      const { user, url, year } = request.body;

      let data = [];
      let roles = await api("roles/roles", url, "GET", {
        conditions: {
          roles_enviados: true,
          __company__: user.data.__company__,
        },
      }, request.headers["access-token"]);
      roles = roles.data.length ? roles.data : [];

      if (roles.length) {
        for (let i in roles) {
          let role_index = await api("roles/roles-index", url, "GET", {
            conditions: {
              rol: roles[i]._id,
              contrato_empleado: user.empleado_id,
            },
          }, request.headers["access-token"]);
          role_index = role_index.data.length ? role_index.data[0] : null;
          if (role_index) {
            let estado = "Sin visualizar";

            if (role_index.view_role) estado = "Visualizado";

            const fecha_rol_ = await fecha_rol({
              url: url,
              rol: roles[i].rol,
              year: roles[i].year,
              month: roles[i].month
            }, request.headers["access-token"]);
            data.push({
              ...roles[i],
              fecha_rol: fecha_rol_,
              estado,
              rol_id: role_index._id,
              recibi_conforme: role_index.recibi_conforme,
              recibi_fecha: role_index.recibi_conforme_date,
              index: {
                ...role_index
              }
            });
          }
        }
      }

      if(data.length) {
        data = data.filter(v => String(v.year) == year)
      }

      return response.status(200).json({
        result: true,
        data: data,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  TiposRoles: async (request = Request, response = Response) => {
    try {
      const { user, url } = request.body;

      let roles = await api("roles-tipos/roles", url, "GET", {
        conditions: {
          __company__: user.data.__company__,
        },
      }, request.headers["access-token"]);
      roles = roles.data.length ? roles.data : [];

      return response.status(200).json({
        result: true,
        data: roles,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  Recibir: async (request = Request, response = Response) => {
    try {
      const { collection, company, parent_company, url } = request.body;

      await api("roles/recibir", url, "POST", {
        config: { company, parent_company },
        collection,
      }, request.headers["access-token"]);
      return response.status(200).json({
        result: true,
        data: true,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  Visualizar: async (request = Request, response = Response) => {
    try {
      const { collection, company, parent_company, url } = request.body;

      await api("roles/visualizar", url, "POST", {
        config: { company, parent_company },
        collection,
      }, request.headers["access-token"]);
      return response.status(200).json({
        result: true,
        data: true,
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  RoleIndex: async (request = Request, response = Response) => {
    try {
      const { url, conditions } = request.body;

      let role_index = await api("roles/roles-index", url, "GET", {
        conditions: {
          ...conditions
        },
      }, request.headers["access-token"]);

      return response.status(200).json({
        result: true,
        data: role_index.data || [],
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
  Roles: async (request = Request, response = Response) => {
    try {
      const { user, url } = request.body;

      let roles = await api("roles/roles", url, "GET", {
        conditions: {
          __company__: user.data.__company__,
        },
      }, request.headers["access-token"]);
      roles = roles.data.length ? roles.data : [];

      return response.status(200).json({
        result: true,
        data: roles
      });
    } catch (error) {
      return response.status(500).json({
        error: "Internal server error. ",
        status: 500,
      });
    }
  },
};
